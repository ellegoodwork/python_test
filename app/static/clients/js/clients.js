$(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('.datatable tfoot th').each( function () {
        var title = $(this).text();
        if($(this).attr('class')!='no_search')
        $(this).html( '<input type="text" style="table-layout: fixed; width:100%;" placeholder="Search '+title+'" />' );
    } );
    
    


var table = $('.datatable').DataTable({
    "oLanguage": oLanguages,
    "bFilter": true,
    "iDisplayLength": 10,
    "aaSorting": [[0, "desc"]],
    "bAutoWidth": true,
    "aoColumns": [
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
        {"bSortable": true, "bSearchable": true, "sClass": "center"},
    ],
    "columnDefs" : [ {
        // 定义操作列,######以下是重点########
        "targets" : 9,//操作按钮目标列
        "data" : null,
        "render" : function(data, type,row) {
            console.log(row)
        var id = '"' + row.id + '"';
        var html = "<a  href=\""+EDIT_URL+"?id="+row[0]+"\" class='up btn btn-default btn-xs'><i class='fa fa-arrow-up'></i> Edit</a>"
          return html;
        }
        } ],
    "bProcessing": true,
    "bServerSide": true,
    "bStateSave": true,
    "sAjaxSource": CLIENTS_LIST_JSON_URL
});
// Apply the search
table.columns().every( function () {
    var that = this;

    $( 'input', this.footer() ).on( 'keyup change clear', function () {
        if ( that.search() !== this.value ) {
            that
                .search( this.value )
                .draw();
        }
    } );
} );

});
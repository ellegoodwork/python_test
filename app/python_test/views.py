from django.shortcuts import render, reverse, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.db.models import Q
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape
from django.views.generic import TemplateView
from .models import Client, ClientForm


def getKwargs(data={}):
    kwargs = {}
    for (k, v) in data.items():
        if v is not None and v != '' and k != 'order':
            print(k)
            print(v)
            kwargs[k] = v
    return kwargs


def index(request):
    kwargs = {}
    if request.method == 'GET':
        kwargs = getKwargs(request.GET)
    if kwargs is not None:
        print(kwargs)
        clients = Client.objects.filter(**kwargs)
    else:
        clients = Client.objects.all()
    if request.GET.get('order'):
        order = request.GET.get('order')
        clients = clients.order_by(order)
    template = loader.get_template('clients/clients.html')
    context = {
        'clients': clients
    }
    return HttpResponse(template.render(context, request))


def client(request):
    instance=None
    if request.GET.get('id'):
        instance = get_object_or_404(Client, id=request.GET.get('id'))
        client = ClientForm(instance=instance)
    else:
        client = ClientForm()
    action = 'update' if instance is not None else 'create'
    template = loader.get_template('clients/client.html')
    context = {
        'client': client,
        'action': action,
    }
    return HttpResponse(template.render(context, request))


def action(request):
    client = None
    if request.POST['action'] == 'update':
        instance = get_object_or_404(Client, id=request.POST['id'])
        client = ClientForm(request.POST or None, instance=instance)
    else:
        client = ClientForm(request.POST)

    if client.is_valid():
        client.save(commit=True)
        return redirect(reverse("clients_list"))

    context = {
        'client': client,
        'action': request.POST['action'],
        'errors': client.errors
    }

    return render_to_response('clients/client.html', context)


class ClientsList(TemplateView):
    template_name = 'clients/clients_list.html'

# This is the search response for https://pypi.org/project/django-datatables-view/
class ClientsModelListJson(BaseDatatableView):
    model = Client

    columns = ['id','client', 'street', 'suburb', 'postcode', 'state', 'contact', 'email', 'phone']
    order_columns = ['id','client', 'suburb', 'state', 'email', 'phone']

    max_display_length = 500

    def filter_queryset(self, qs): 
        sSearch_1 = self.request.GET.get('sSearch_1', None)
        sSearch_2 = self.request.GET.get('sSearch_2', None)
        sSearch_3 = self.request.GET.get('sSearch_3', None)
        sSearch_4 = self.request.GET.get('sSearch_4', None)
        sSearch_5 = self.request.GET.get('sSearch_5', None)
        sSearch_6 = self.request.GET.get('sSearch_6', None)
        sSearch_7 = self.request.GET.get('sSearch_7', None)
        sSearch_8 = self.request.GET.get('sSearch_8', None)
        sSearch = self.request.GET.get('sSearch', None)
        if sSearch_1:
            qs = qs.filter(
                Q(client__istartswith=sSearch_1))
            return qs
        elif sSearch_2:
            qs = qs.filter(
                Q(street__istartswith=sSearch_2))
            return qs
        elif sSearch_3:
            qs = qs.filter(
                Q(suburb__istartswith=sSearch_3))
            return qs
        elif sSearch_4:
            qs = qs.filter(
                Q(postcode__istartswith=sSearch_4))
            return qs
        elif sSearch_5:
            qs = qs.filter(
                Q(state__istartswith=sSearch_5))
            return qs
        elif sSearch_6:
            qs = qs.filter(
                Q(contact__istartswith=sSearch_6))
            return qs
        elif sSearch_7:
            qs = qs.filter(
                Q(email__istartswith=sSearch_7))
            return qs
        elif sSearch_8:
            qs = qs.filter(
                Q(phone__istartswith=sSearch_8))
            return qs
        elif sSearch:
            qs = qs.filter(
                Q(client__istartswith=sSearch) | Q(street__istartswith=sSearch) | Q(suburb__istartswith=sSearch) | Q(postcode__istartswith=sSearch) | 
                Q(state__istartswith=sSearch) | Q(contact__istartswith=sSearch) | Q(email__istartswith=sSearch)  | Q(phone__istartswith=sSearch))
            return qs
        return qs
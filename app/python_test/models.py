from django.db import models
from django import forms

class Client(models.Model):
    client = models.CharField(unique=True, max_length=64)
    street = models.CharField(max_length=64, null=True, blank=True)
    suburb = models.CharField(max_length=64, null=True, blank=True)
    postcode = models.CharField(max_length=64, null=True, blank=True)
    state = models.CharField(max_length=64, null=True, blank=True)
    contact = models.CharField(max_length=64, null=True, blank=True)
    email = models.CharField(max_length=64)
    phone = models.CharField(max_length=64)

class ClientForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput())
    postcode = forms.RegexField(regex=r'^\d{4,10}$', required=False)
    email = forms.EmailField()
    phone = forms.RegexField(regex=r'^\d{4,13}$')
    class Meta:
        model = Client
        fields = ['id', 'client', 'street', 'suburb', 'postcode', 'state', 'contact', 'email', 'phone']


"""python_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^old_clients/', views.index, name='index'),
                  url(r'^client/action$', views.action, name='action'),
                  url(r'^client$', views.client, name='client'),
                  url(r'^$', views.ClientsList.as_view(), name="clients_list"),
                  url(r'^clients_json/', views.ClientsModelListJson.as_view(), name='clients_json'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

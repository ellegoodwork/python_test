# Client Demo
For this test, I just go through all the requirements. It is a basic test for crud, so, I try to add a docker for deployment. The first version was added by Django 2 with raw template code. But
then I realise maybe the test require Django 1 and also I add https://addressfinder.com.au/ + https://pypi.org/project/django-datatables-view/ for better user experience. 
## Todo
    Just list some parts can be improved:
    1. Testing, CI/CD, and Site Reliability
    2. Can have a better frontend design;
    3. May be now it is a trend to have API & React 
    4. Elasticsearch or CloudSearch support
    5. deploy to a demo server
## Installation
### step1
* docker-compose up -d
* docker-compose run --rm app /opt/app/manage.py migrate

for dev, you may need: docker-compose build
### step2
view http://127.0.0.1:8000 this version was added with Datatable view
I also add a raw list http://127.0.0.1:8000/client








# WebIT Django assessment

## Purpose

To check that your knowledge/research ability to use the Django framework meets what's required of a python role with WebIT.

### Assessments

1. Given the code in this repository, create a model to store a "Client", a client contains:
    * a client name
    * their address (as street name, suburb, postcode and state)
    * a contact name
    * an email address
    * a phone number
Notes: the most commonly queried fields are suburb, client name and email address. There should be no duplicate client names.

2. Write a view to create a new Client via a basic HTML form. Required fields for a client include:
    *  client name
    *  email address
    *  phone number


3. Using the view from 2. as a basis. Add another view that can update a given Client record.

4. Create a view to list all Client records, including a search form. The view must include the following:
    * a search by client name
    * a search by email address
    * a search by phone number
    * a search by client suburb
    * the ability to order by name, email address, phone number and suburb
    * each record should link to the update view from 3.
